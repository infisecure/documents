Steps to integrate InfiSecure – 
	
	1. Copy the infisecure_config.php file and infisecure_service.php file to your project
	
	2. Add below code to the common header file of your website. Alternatively, you can add the code at all individual places where you want to call InfiSecure service.
			<?php
				include 'infisecure_service.php';
				$requested_by = "";
				try {
						$infisecure_response=validateRequest($requested_by);
				}catch(Exception $e) {
						print_r($e->getTraceAsString());
				}
				if($infisecure_response->responseCode == '1003' || $infisecure_response->responseCode == '1001')
				{
						// PHP permanent URL redirection
						header("Location: ".$infisecure_response->redirectPage, true, 301);
						exit();
				}
			?>
			
	3. Add the below code to the common footer or individual pages
			<div id="isjshk" style="display:none">9da1ad3e43c464e3c854b809c369a076</div>
			<script type="text/javascript">
				var upid = "<?php echo $infisecure_response->upid?>";
			</script>
			<script type="text/javascript" src="https://cdn.infisecure.com/vietjet.js"></script>
			
I have also provided sample.php for your reference if in case you want to test using sample.php file.