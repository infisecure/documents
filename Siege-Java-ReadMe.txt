INFISECURE JAVA INTEGRATION GUIDE
=================================

InfiSecure "siege-java" module is a custom servlet filter which intercepts the request and response and sends it to InfiSecure Bot mitigation servers to detect bot traffic.

Before the regular servlet process, the module makes a call to the InfiSecure API using a keepalive connection. Depending on the API response, the module will either block the query or let server continue the regular process.

The module has been developed to protect user experience as if any error was to occur during the process or if the timeout was reached, the module would automatically disable its blocking process and allow those hits.
Pre-Requisites
	1. Java Version >= 1.7
	2. Apache Maven

STEPS TO INTEGRATE
==================

	1. Copy the isconfig.properties to the src/main/resources folder and modify the variables as per individual comments.

	2. Add the SiegeV2-0.0.1-RELEASE to classpath
	
	3. Once you have completed the above step, copy paste the below code in the controller or write a filter with the below code to allow or block the request.
		ISiegeService siegeService = new SiegeServiceImpl();
		JsonNode jsonNode = siegeService.validateRequest(request, response, ""); // method params HttpServletRequest, HttpServletResponse, hashed user id or empty string
		request.setAttribute("infiresponse", jsonNode);
		
		if(jsonNode.get("statusCode").equals("1001")){
			//action to be taken="throw captcha";
			
		}
		else if(jsonNode.get("statusCode").equals("1002")){
			//action to be taken="feed fake data";
			
		}
		else if(jsonNode.get("statusCode").equals("1003")){
			//action to be taken="block or render a blank page or 404 page";
			
		}
		else{
			//action to be taken="allow";
		}
		
	On integrating the code, the InfiSecure integrator will start making API calls to InfiSecure and the response received is in the form of a response code. The response code directly indicates the action to be taken based on the incoming request.
	
	4. Add below code to the common header file of your website. Alternatively, you can add the code at all individual pages.
		<div id="isjshk" style="display:none">9da1ad3e43c464e3c854b809c369a076</div>
		<script type="text/javascript">
			var upid = infiresponse.upid;
		</script>
		<script type="text/javascript" src="https://cdn.infisecure.com/coopculture.js"></script>
		
FAQs
====
List of infisecure status codes
	a. 1000 → allow
	b. 1001 → captcha 
	c. 1002 → feed fake data
	d. 1003 → block
	e. 1004 → fake traffic
	f. 1005 → monitor
	g. -1 → exception
	h. 1011 → subscription expired